﻿using System;
using System.IO;
using System.Net;
using Alanta.Smtp.ContentProcessing;
using Alanta.Smtp.Delivery;
using Alanta.Smtp.Logging;
using Alanta.Smtp.Routing;

namespace Alanta.Smtp.Selfhosted
{
    class Program
    {
        static int Main( string[] args )
        {
           var log = new ConsoleLogger();

           var options = new Options();
           if ( !CommandLine.Parser.Default.ParseArguments( args, options ) )
           {
              // Values are available here
              return 1;
           }

            var outputFolder = options.OutputPath;
            if (string.IsNullOrWhiteSpace(outputFolder))
            {
                outputFolder = "output";
            }

            if (!Path.IsPathRooted(outputFolder))
            {
                outputFolder = Path.Combine(System.Environment.CurrentDirectory, outputFolder);
            }

            if (!Directory.Exists(outputFolder))
            {
                try
                {
                    Directory.CreateDirectory(outputFolder);
                }
                catch (Exception ex)
                {
                    log.Error("Can't create output directory: " + outputFolder);
                    return 1;
                }
            }

            var sessionFactory = new DefaultSmtpSessionFactory();
            sessionFactory.Routes.Add(
                new DeliveryAgentRoute(
                    new FileDeliveryAgent(new TraceInformation(new Alanta.Smtp.Dns.WindowsDnsResolver()), outputFolder)));
           var ipAddress = IPAddress.Loopback;

           if (!string.IsNullOrWhiteSpace(options.CertificateFile))
           {
              var fullPath = System.IO.Path.GetFullPath(options.CertificateFile);
              if (!File.Exists(fullPath))
              {
                 log.Error( "Can't find certificate file: " + fullPath );
                 return 1;
              }

              try
              {
                 sessionFactory.Certificate = Smtp.Tls.Certificates.LoadFromPem(File.ReadAllText(fullPath));
              }
              catch (Exception ex)
              {
                 log.Error("Failed to load certificate from file:" + fullPath);
                 log.Error( ex.ToString() );
                 return 1;
              }
           }

           if (!string.IsNullOrWhiteSpace(options.BindAddress))
           {
              if (!IPAddress.TryParse(options.BindAddress, out ipAddress))
              {
                 log.Error( "Unable to parse IP address to bind to :" + options.BindAddress );
                 return 1;
              }
           }

           var server = new Smtp.SmtpServer(
              sessionFactory,
              log,
              ipAddress, options.Port ?? 25 );


           log.Info("Starting SMTP server on {0} port {1}", ipAddress.ToString(), options.Port ?? 25);
            server.Start();

            Console.WriteLine( "Hit enter to stop.");
            Console.ReadLine();

            server.Stop();

           return 0;
        }
    }
}
